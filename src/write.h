#include <errno.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <time.h>
// IMPORTANT:
// http://stackoverflow.com/questions/3355298/unistd-h-and-c99-on-linux
// #define _XOPEN_SOURCE 500
#include <unistd.h>


#include "random_utils.h"


#define FILENAME_LENGTH 512

void Write_init(int cuda_device);
void Write_active_node_count_average (double *active_node_count, int *t_calcs, int len,
                                      float lambda, float nu, float K, float rho,
                                      int time_steps, int repeat);

void Write_survival_probability (double *survival_probability, int *t_calcs, int len,
                                 float lambda, float nu, float K, float rho,
                                 int time_steps, int repeat);

void Write_avalanche_distribution(long *avalanche_distribution, int len,
                                  float lambda, float nu, float K, float rho,
                                  int time_steps, int repeat);

int cuda_device_ (int cuda_device);
void get_hostname_ (char * hostname);
void get_datetime_(char * dest);
void get_first_datetime_ (char * first_datetime);
void write_double_array(int *tt, double *x, int len, int time_steps, char filename[FILENAME_LENGTH]);

void Write_init(int cuda_device) {
    char dc[32];
    get_first_datetime_(dc);
    cuda_device_(cuda_device);
}


void Write_active_node_count_average (double *active_node_count, int *t_calcs, int len,
                                      float lambda, float nu, float K, float rho,
                                      int time_steps, int repeat) {
    char first_datetime[32]; get_first_datetime_(first_datetime);
    char hostname[32]; get_hostname_(hostname);
    char datetime[32]; get_datetime_(datetime);

    char directory[64] = "average-active-nc_";
    strcat(directory, hostname);
    mkdir(directory, 0755);

    char *ext = "dat";
    char filename[FILENAME_LENGTH];

    sprintf(filename,
            "%s/ps-%s_dev-%d_"
            "lambda-%.3f_nu-%.3f_K-%.3f_rho-%.3f_"
            "T-%d_R-%d_"
            "X-%s_rnd-%d.%s",
            directory, first_datetime, cuda_device_(-1),
            lambda, nu, K, rho,
            time_steps, repeat,
            datetime, RandomUtils_get_random_long(99), ext);

    write_double_array(t_calcs, active_node_count, len, time_steps, filename);
}


void Write_survival_probability (double *survival_probability, int *t_calcs, int len,
                                 float lambda, float nu, float K, float rho,
                                 int time_steps, int repeat) {
    char first_datetime[32]; get_first_datetime_(first_datetime);
    char hostname[32]; get_hostname_(hostname);
    char datetime[32]; get_datetime_(datetime);

    char directory[64] = "survival-probability_";
    strcat(directory, hostname);
    mkdir(directory, 0755);

    char *ext = "dat";
    char filename[FILENAME_LENGTH];

    sprintf(filename,
            "%s/ps-%s_dev-%d_"  // program started, cuda device
            "lambda-%.3f_nu-%.3f_K-%.3f_rho-%.3f_"
            "T-%d_R-%d_"
            "X-%s_rnd-%d.%s",
            directory, first_datetime, cuda_device_(-1),
            lambda, nu, K, rho,
            time_steps, repeat,
            datetime, RandomUtils_get_random_long(99), ext);

    write_double_array(t_calcs, survival_probability, len, time_steps, filename);
}


void Write_avalanche_distribution(long *avalanche_distribution, int len,
                                  float lambda, float nu, float K, float rho,
                                  int time_steps, int repeat) {
    //
    char first_datetime[32]; get_first_datetime_(first_datetime);
    char hostname[32]; get_hostname_(hostname);
    char datetime[32]; get_datetime_(datetime);

    char directory[64] = "avalanche-distro_";
    strcat(directory, hostname);
    mkdir(directory, 0755);

    char *ext = "dat";
    char filename[FILENAME_LENGTH];

    sprintf(filename,
            "%s/ps-%s_dev-%d_"  // program started, cuda device
            "lambda-%.3f_nu-%.3f_K-%.3f_rho-%.3f_"
            "T-%d_R-%d_"
            "X-%s_rnd-%d.%s",
            directory, first_datetime, cuda_device_(-1),
            lambda, nu, K, rho,
            time_steps, repeat,
            datetime, RandomUtils_get_random_long(99), ext);

    FILE *fp;
    fp = fopen(filename, "wb");
    for (int i = 0; i < len; i++) {
        fprintf(fp, "%ld\n", avalanche_distribution[i]);
    }
    fclose(fp);
}


int cuda_device_ (int cuda_device) {
    static bool initialized = false;
    static int m_cuda_device;
    if (!initialized)
        m_cuda_device = cuda_device;
    initialized = true;
    return m_cuda_device;
}


void get_hostname_ (char * hostname) {
    static bool initialized = false;
    static char h[32];
    if (!initialized)
        gethostname(h, sizeof(h));
    initialized = true;
    strcpy(hostname, h);

}


void get_datetime_(char * dest) {
    const int len = 18;
    char datetime[len];
    time_t now = time(NULL);
    if (now != -1) {
        strftime(datetime, len, "%y-%m-%d_%H-%M-%S", localtime(&now));
        strcpy(dest, datetime);
    }
}


void get_first_datetime_ (char * first_datetime) {
    // identifies our first run
    static bool initialized = false;
    static char dt[32];
    if (!initialized)
        get_datetime_(dt);
    initialized = true;
    strcpy(first_datetime, dt);
}


void write_double_array(int *tt, double *x, int len, int time_steps, char filename[FILENAME_LENGTH]){
    FILE *fp;
    fp = fopen(filename, "wb");
    for (int i = 0; i < len && tt[i] < time_steps; i++) {
        if (!x[i])  // Stop after first 0 value
            break;
        fprintf(fp, "%d %f\n", tt[i], x[i]);
    }
    fclose(fp);
}
