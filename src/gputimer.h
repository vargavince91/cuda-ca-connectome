#ifndef GPUTIMER_H_INCLUDED
#define GPUTIMER_H_INCLUDED


// This is C++


struct GpuTimer {
    /*https://github.com/aramadia/udacity-cs344/blob/master/Unit2%20Code%20Snippets/gputimer.h*/
    cudaEvent_t _start;
    cudaEvent_t _stop;

    GpuTimer() {  // constructor
        cudaEventCreate(&_start);
        cudaEventCreate(&_stop);
    }

    ~GpuTimer() {  // destructor
        cudaEventDestroy(_start);
        cudaEventDestroy(_stop);
    }

    void start() {
        cudaEventRecord(_start, 0);
    }

    void stop() {
        cudaEventRecord(_stop, 0);
    }

    float elapsed() {
        float _elapsed;
        cudaEventSynchronize(_stop);
        cudaEventElapsedTime(&_elapsed, _start, _stop);
        return _elapsed;
    }
};


#endif
