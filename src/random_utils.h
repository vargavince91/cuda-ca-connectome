#ifndef RANDOM_UTILS_H_INCLUDED
#define RANDOM_UTILS_H_INCLUDED


#include <stdbool.h>
#include <stddef.h>
#include <stdlib.h>
#include <sys/time.h>
#include <time.h>

#include <cuda.h>
#include <curand.h>

#include "safe_calls.h"


int RandomUtils_get_seed();
int RandomUtils_get_random_long(int max);
float RandomUtils_get_random_float();
curandGenerator_t RandomUtils_create_generator();
void RandomUtils_destroy_generator(curandGenerator_t generator);


static bool seed_used_ = false;


int RandomUtils_get_seed() {
    seed_used_ = true;
    struct timeval timeValueStructure;
    gettimeofday(&timeValueStructure, NULL);
    return (int) timeValueStructure.tv_usec;
}


// http://stackoverflow.com/questions/2509679/how-to-generate-a-random-number-from-within-a-range
// Assumes 0 <= max <= RAND_MAX
// Returns in the closed interval [0, max]
int RandomUtils_get_random_long(int max) {
    if (!seed_used_)
        srand(RandomUtils_get_seed());

    unsigned long
    // max <= RAND_MAX < ULONG_MAX, so this is okay.
            num_bins = (unsigned long) max + 1,
            num_rand = (unsigned long) RAND_MAX + 1,
            bin_size = num_rand / num_bins,
            defect   = num_rand % num_bins;

    int x;
    do {
        x = rand();
    } while (num_rand - defect <= (unsigned long)x);  // This is carefully written not to overflow

    return x/bin_size;  // Truncated division is intentional
}


float RandomUtils_get_random_float() {

    if (!seed_used_)
        srand(RandomUtils_get_seed());

    return (float)rand()/(float)RAND_MAX;
}


curandGenerator_t RandomUtils_create_generator() {
    int seed = RandomUtils_get_seed();
    curandGenerator_t generator;
    curandSafe(curandCreateGenerator(&generator, CURAND_RNG_PSEUDO_MRG32K3A));
    curandSafe(curandSetPseudoRandomGeneratorSeed(generator, seed));
    return generator;
}


void RandomUtils_destroy_generator(curandGenerator_t generator) {
    curandSafe(curandDestroyGenerator(generator));
}


#endif
