#ifndef ARGPARSE_UTILS_H_INCLUDED
#define ARGPARSE_UTILS_H_INCLUDED


#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>


#include "ascii_utils.h"


#define DEFAULT_CUDA_DEVICE 1


int ArgparseUtils_parse(int argc, char **argv, char **filename, int *cuda_device);
bool util_strtoi_10_(const char* src, int *dest);
int file_exists_ (char *filename);


int ArgparseUtils_parse(int argc, char **argv,
                        char **filename, int *cuda_device) {
    char usage[512] =
            "USAGE: <.out> filename [cuda_device]\n"
            "\tfilename: four-columned .dat file i: from_node, ii: to_node, iii: weight, iv: in_largest_component\n"
            "\tcuda_device: [optional] device number to run simulations on";
    int failed = 0;

    if (argc >= 2) {
        *filename = argv[1];
        if (!file_exists_(*filename) && ++failed)
            AsciiUtils_fatal_error_msg("File doesn't exist");

        if (argc > 2) {
            bool s = util_strtoi_10_(argv[2], cuda_device);
            if (!s && ++failed)
                AsciiUtils_fatal_error_msg("Couldn't parse: <cuda_device>");
        } else {
            *cuda_device = DEFAULT_CUDA_DEVICE;
        }

        return failed ? EXIT_FAILURE : EXIT_SUCCESS;
    } else {
        AsciiUtils_fatal_error_msg(usage);
        return EXIT_FAILURE;
    }
}


bool util_strtoi_10_(const char* src, int *dest) {
    char *endptr;
    *dest = (int) strtol(src, &endptr, 10);
    return src != endptr;
}


int file_exists_ (char *filename) {
    // http://stackoverflow.com/questions/230062/whats-the-best-way-to-check-if-a-file-exists-in-c-cross-platform
    // R_OK from unistd.h
    return access(filename, R_OK) != -1;
}


#endif