#ifndef SAFE_CALLS_H_INCLUDED
#define SAFE_CALLS_H_INCLUDED


#include <stdio.h>
#include <cuda.h>

#include "curand_utils.h"

//http://stackoverflow.com/questions/14038589/what-is-the-canonical-way-to-check-for-errors-using-the-cuda-runtime-api
// Macro to catch CUDA errors in CUDA runtime calls
#define cuSafe(ans) { gpuCuSafe((ans), __FILE__, __LINE__); }
inline void gpuCuSafe(cudaError_t code, const char *file, int line) {
    if (code != cudaSuccess) {
        fprintf(stderr,
                "GPU ERROR: '%s' in file %s (line %d)\n",
                cudaGetErrorString(code),
                file,
                line);
        exit(EXIT_FAILURE);  // or code would be better?
    }
}

// http://docs.nvidia.com/cuda/curand/host-api-overview.html#axzz4QRovrhiM
#define curandSafe(ans) { gpuCuRandSafe((ans), __FILE__, __LINE__);}
inline void gpuCuRandSafe(curandStatus_t status, const char *file, int line) {
    if (status != CURAND_STATUS_SUCCESS) {
        fprintf(stderr,
                "GPU CURAND ERROR: '%s' in file %s (line %d)\n",
                curandGetStatusString(status),
                file,
                line);
        exit(EXIT_FAILURE);
    }
}


#endif
