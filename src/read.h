#include <stdlib.h>

#include "random_utils.h"


void Read_network(char *filename, float rho, float kappa,
                  int *node_count, int *edge_count,
                  int ***in_network, float ***in_network_weight);


int get_node_count_(char *filename);
void fill_int_network_(char *filename, int node_count, int *edge_count,
                       float rho, int ***in_network, int ***in_network_weight);
void alloc_float_network_ (int node_count, int **in_network_weight_int,
                           float ***in_network_weight_float);
void int_to_float_rel_(int node_count, int **from_int_in_network_weight,
                       float ***to_float_in_network_weight);
void inhibitory_edges_(int node_count, float ***in_network_weight, float kappa);


void Read_network(char *filename, float rho, float kappa,
                  int *node_count, int *edge_count,
                  int ***in_network, float ***in_network_weight) {
    int **in_network_weight_tmp;
    *node_count = get_node_count_(filename);
    fill_int_network_(filename, *node_count, edge_count, rho,
                      in_network, &in_network_weight_tmp);
    alloc_float_network_(*node_count, in_network_weight_tmp, in_network_weight);
    int_to_float_rel_(*node_count, in_network_weight_tmp, in_network_weight);
    inhibitory_edges_(*node_count, in_network_weight, kappa);
}


int get_node_count_ (char *filename) {
    int from_node, to_node, last_node = -1;
    FILE *stream_file_pointer;
    stream_file_pointer = fopen(filename, "r");
    while (fscanf(stream_file_pointer, "%d %d %*d %*d\n", &from_node, &to_node) > 0) {
        if (last_node < from_node)
            last_node = from_node;
        if (last_node < to_node)
            last_node = to_node;
    }
    fclose(stream_file_pointer);
    return last_node + 1; // node index starts with 0, thus the +1
}


void fill_int_network_ (char *filename, int node_count, int *edge_count,
                        float rho, int ***in_network, int ***in_network_weight){
    *edge_count = 0;  // make sure it's initialized
    *in_network = (int **) malloc(node_count * sizeof(int *));
    *in_network_weight = (int **) malloc(node_count * sizeof(int *));

    for (int i = 0; i < node_count; i++) {
        (*in_network)[i] = (int *) malloc(1 * sizeof(int));
        (*in_network_weight)[i] = (int *) malloc(1 * sizeof(int));
    }

    for (int i = 0; i < node_count; i++) {
        (*in_network)[i][0] = 0;
        (*in_network_weight)[i][0] = 0;
    }

    FILE *stream_file_pointer;
    stream_file_pointer = fopen(filename, "r");
    int a_node, b_node, weight, in_large_component;

    while (fscanf(stream_file_pointer, "%d %d %d %d\n", &a_node, &b_node, &weight, &in_large_component) > 0) {
        if (!in_large_component)
            continue;

        bool start_with_first = RandomUtils_get_random_float() <= 0.5;
        bool add_both_ways = RandomUtils_get_random_float() <= rho;

        *edge_count += add_both_ways ? 2 : 1;

        int from_node = start_with_first ? a_node : b_node;
        int to_node = start_with_first ? b_node : a_node;

        // TODO: could be put in a function
        // increase length of the array by one
        (*in_network)[from_node] = (int *) realloc((*in_network)[from_node],
                                                ((*in_network)[from_node][0] + 2) * sizeof(int));
        // increase the number of nodes
        (*in_network)[from_node][0] += 1;
        // Get the index that points to the last (new) element of the array and
        // Append the index of the destination node
        (*in_network)[from_node][(*in_network)[from_node][0]] = to_node;



        // increase length of the array by one
        (*in_network_weight)[from_node] = (int *) realloc((*in_network_weight)[from_node], ((*in_network_weight)[from_node][0] + 1 + 1) * sizeof(int));
        // increase the number of nodes
        (*in_network_weight)[from_node][0] += 1;
        // Get the index that points to the last (new) element of the array and
        // Append the index of the destination node
        (*in_network_weight)[from_node][(*in_network_weight)[from_node][0]] = weight;

        if (add_both_ways) {
            // Switch them, so it's easier to copy-paste
            int tmp = from_node;
            from_node = to_node;
            to_node = tmp;

            // increase length of the array by one
            (*in_network)[from_node] = (int *) realloc((*in_network)[from_node], ((*in_network)[from_node][0] + 1 + 1) * sizeof(int));
            // increase the number of nodes
            (*in_network)[from_node][0] += 1;
            // Get the index that points to the last (new) element of the array
            // Append the index of the destination node
            (*in_network)[from_node][(*in_network)[from_node][0]] = to_node;

            // increase length of the array by one
            (*in_network_weight)[from_node] = (int *) realloc((*in_network_weight)[from_node], ((*in_network_weight)[from_node][0] + 1 + 1) * sizeof(int));
            // increase the number of nodes
            (*in_network_weight)[from_node][0] += 1;
            // Get the index that points to the last (new) element of the array
            // Append the index of the destination node
            (*in_network_weight)[from_node][(*in_network)[from_node][0]] = weight;
        }
    }
    fclose(stream_file_pointer);
}


void alloc_float_network_ (int node_count, int ** in_network_weight_int,
                           float *** in_network_weight_float) {
    *in_network_weight_float = (float **) malloc(node_count * sizeof(float *));
    for (int i = 0; i < node_count; i ++)
        (*in_network_weight_float)[i] = (float *) malloc((in_network_weight_int[i][0] + 1) * sizeof(float));
}


void int_to_float_rel_(int node_count, int **from_int_in_network_weight,
                       float ***to_float_in_network_weight) {
    for (int i = 0; i < node_count; i++) {
        int sum_weights = 0;
        for (int j = 1; j <= from_int_in_network_weight[i][0]; j++)
            sum_weights += from_int_in_network_weight[i][j];
        (*to_float_in_network_weight)[i][0] = from_int_in_network_weight[i][0];
        for (int j = 1; j <= from_int_in_network_weight[i][0]; j++)
            (*to_float_in_network_weight)[i][j] = (float) from_int_in_network_weight[i][j] / (float) sum_weights;
    }
}

void inhibitory_edges_(int node_count, float ***in_network_weight, float kappa){
    int i, j;
    for (i = 0; i < node_count; i++)
        for (j = 1; j <= (*in_network_weight)[i][0]; j++)
            if (RandomUtils_get_random_float() <= kappa)
                (*in_network_weight)[i][j] = -(*in_network_weight)[i][j];
}
