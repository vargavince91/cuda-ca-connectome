#include <stdlib.h>


void Linearise_network(int node_count, int edge_count,
                       int **in_network, float **in_network_weight,
                       int **position, int **network, float **network_weight);


void allocate_lin_networks_(int node_count, int edge_count,
                            int **position,
                            int **network, float **network_weight);
void linearise_int_ (int node_count, int edge_count,
                     int **in_network,
                     int *position, int *network);
void linearise_float_ (int node_count, int edge_count,
                       float ** in_network,
                       int * position, float * network);
void print_int_ (int *position, int *network,
                 int node_count, int edge_count);
void print_float_ (int *position, float * network,
                   int node_count, int edge_count);


void Linearise_network(int node_count, int edge_count,
                       int **in_network, float **in_network_weight,
                       int **position, int **network, float **network_weight) {
    allocate_lin_networks_(node_count, edge_count, position, network, network_weight);
    linearise_int_(node_count, edge_count, in_network, *position, *network);
    linearise_float_(node_count, edge_count, in_network_weight, *position, *network_weight);
}


void allocate_lin_networks_(int node_count, int edge_count,
                            int **position,
                            int **network, float **network_weight) {
    *position = (int *) malloc(node_count * sizeof(int));
    *network = (int *) malloc((node_count + edge_count)* sizeof(int));
    *network_weight = (float *) malloc((node_count + edge_count)* sizeof(float));
}

void linearise_int_ (int node_count, int edge_count,
                     int **in_network,
                     int *position, int *network) {
    for (int node = 0, i = 0; node < node_count; node++)
        for (int edge = 0; edge <= in_network[node][0]; edge++, i++){
            if (edge == 0)
                position[node] = i;
            network[i] = in_network[node][edge];
        }
}

void linearise_float_ (int node_count, int edge_count,
                       float ** in_network,
                       int * position, float * network) {
    for (int node = 0, i = 0; node < node_count; node++)
        for (int edge = 0; edge <= in_network[node][0]; edge++, i++){
            if (edge == 0)
                position[node] = i;
            network[i] = in_network[node][edge];
        }
}

void print_int_ (int *position, int *network,
                 int node_count, int edge_count) {
    for (int pos = 0, node = 0; pos < node_count + edge_count; pos++) {
        if (position[node] == pos)
            printf("\nNode %d\t Count: ", node++);
        printf("%d\t", network[pos]);
    }
    printf("\n");
}

void print_float_ (int *position, float * network,
                   int node_count, int edge_count) {
    for (int pos = 0, node = 0; pos < node_count + edge_count; pos++) {
        if (position[node] == pos)
            printf("\nNode %d\t Count: %1.0f\t", node++, network[pos]);
        else
            printf("%1.3f\t", network[pos]);
    }
    printf("\n");
}
