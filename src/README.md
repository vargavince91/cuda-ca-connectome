1. Go to `source_code` folder. `cd source_code`
2. Fetch data file. `curl http://www.energia.mta.hu/~odor/pub/Awr.dat > d.dat`
3. Compile, e.g. `nvcc -o program.o -lm -lcuda -lcurand main.cu` or  you may add optional flags `nvcc -O3 -o program.o -lm -lcuda -lcurand -arch sm_20 main.cu`
4. Run program. `program.o d.dat 0`. The first parameter is the name of the network's data file. The second parameter is to set the CUDA device ID (run `nvidia-smi` for more information about the available GPUs of your computer).

The cellular automata and spreading are in the `main.cu` file.
