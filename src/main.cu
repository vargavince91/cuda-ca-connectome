#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <argp.h>

#include <cuda.h>
#include <curand.h>

// utils
#include "safe_calls.h"
#include "curand_utils.h"
// TODO FIX
//#include "argparse_utils.h"
#include "ascii_utils.h"
#include "gputimer.h"
#include "random_utils.h"

// logic
#include "read.h"
#include "linearise.h"
#include "write.h"

#define NODE_ACTIVE 1
#define NODE_INACTIVE 0

/*
 * HANDLE ARGUMENTS AND OPTIONS
 */
 // TODO: move this whole argument handler section into a separate file?

const char *argp_program_version = "cuda-ca-connectome 0.0.1";

const char *argp_program_bug_address =
    "https://gitlab.com/vargavince91/cuda-ca-connectome/issues";

static char doc[] =
    "Spreading simulations using cellular automata model on connectome networks, written in CUDA C. "
    "For more information, go to https://gitlab.com/vargavince91/cuda-ca-connectome";

#define REQUIRED_ARGS 1
#define ARG_FILENAME 0

static char args_doc[] = "FILENAME";

static struct argp_option options[] = {
    {"from-lambda",       'l', "float[0,1]", 0, "Description LATER"},
    {"to-lambda",         'L', "float[0,1]", 0, "Description LATER"},
    {"delta-lambda",      'M', "float[0,1]", 0, "Description LATER"},
    {"from-nu",           'n', "float[0,1]", 0, "Description LATER"},
    {"to-nu",             'N', "float[0,1]", 0, "Description LATER"},
    {"delta-nu",          'U', "float[0,1]", 0, "Description LATER"},
    {"from-theta",        't', "float[0,1]", 0, "Description LATER"},
    {"to-theta",          'T', "float[0,1]", 0, "Description LATER"},
    {"delta-theta",       'E', "float[0,1]", 0, "Description LATER"},
    {"from-rho",          'r', "float[0,1]", 0, "Description LATER"},
    {"to-rho",            'R', "float[0,1]", 0, "Description LATER"},
    {"delta-rho",         'O', "float[0,1]", 0, "Description LATER"},
    {"from-kappa",        'k', "float[0,1]", 0, "Description LATER"},
    {"to-kappa",          'K', "float[0,1]", 0, "Description LATER"},
    {"delta-kappa",       'P', "float[0,1]", 0, "Description LATER"},
    {"device",            'd', "int", 0, "CUDA device to execute the computation on. To see options, run nvidia-smi command."},
    {"repeat",            'x', "int", 0, "Description LATER"},
    {"steps",             's', "int", 0, "Description LATER"},
    {"threads-per-block", '#', "int", 0, "Find best value for your device and modify it accordingly. Default value 128."},
    {"verbose",           'v', 0, 0, "Produce verbose output"},
    {"quiet",             'q', 0, 0, "Don't produce any output"},
    {0}
};

struct arguments {
    /* Arguments */
    // For the source graph's filename
    char *args[REQUIRED_ARGS];
    /* Options */
    // lambda: Activation probability
    float from_lambda,
        to_lambda,
        delta_lambda;
    // nu: Deactivation probability
    float from_nu,
        to_nu,
        delta_nu;
    // theta: Threshold
    float from_theta,
        to_theta,
        delta_theta;
    // rho: Edge probability
    float from_rho,
        to_rho,
        delta_rho;
    // kappa: Inhibitory edge probability
    float from_kappa,
        to_kappa,
        delta_kappa;
    int device;
    int repeat;
    int steps;
    int threads_per_block;
    // Verbosity controls
    int quiet, verbose;
};

static error_t parse_opt (int key, char *arg, struct argp_state *state) {
    struct arguments *arguments = (struct arguments*) state->input;
    switch (key) {
        case 'l': arguments->from_lambda = strtof(arg, NULL); break;
        case 'L': arguments->to_lambda = strtof(arg, NULL); break;
        case 'M': arguments->delta_lambda = strtof(arg, NULL); break;
        case 'n': arguments->from_nu = strtof(arg, NULL); break;
        case 'N': arguments->to_nu = strtof(arg, NULL); break;
        case 'U': arguments->delta_nu = strtof(arg, NULL); break;
        case 't': arguments->from_theta = strtof(arg, NULL); break;
        case 'T': arguments->to_theta = strtof(arg, NULL); break;
        case 'E': arguments->delta_theta = strtof(arg, NULL); break;
        case 'r': arguments->from_rho = strtof(arg, NULL); break;
        case 'R': arguments->to_rho = strtof(arg, NULL); break;
        case 'O': arguments->delta_rho = strtof(arg, NULL); break;
        case 'k': arguments->from_kappa = strtof(arg, NULL); break;
        case 'K': arguments->to_kappa = strtof(arg, NULL); break;
        case 'P': arguments->delta_kappa = strtof(arg, NULL); break;
        case 'd': arguments->device = (int) strtol(arg, NULL, 10); break;
        case 's': arguments->steps = (int) strtol(arg, NULL, 10); break;
        case 'x': arguments->repeat = (int) strtol(arg, NULL, 10); break;
        case '#': arguments->threads_per_block = (int) strtol(arg, NULL, 10); break;
        case 'q': arguments->quiet = 1; break;
        case 'v': arguments->verbose = 1; break;
        case ARGP_KEY_ARG:
            if (state->arg_num >= REQUIRED_ARGS)  // Too many arguments
                argp_usage(state);
            arguments->args[state->arg_num] = arg; break;
        case ARGP_KEY_END:
            if (state->arg_num < REQUIRED_ARGS)  // Not enough arguments.
                argp_usage(state); break;
        default: return ARGP_ERR_UNKNOWN;
    }
    return 0;
}

static struct argp argp = {options, parse_opt, args_doc, doc};

#define min(a,b) \
   ({ __typeof__ (a) _a = (a); \
       __typeof__ (b) _b = (b); \
     _a < _b ? _a : _b; })

void spreading(int node_count, int edge_count, int init_node,
        int *d_position, int *d_network, float *d_network_weight, int steps,
        float K, float flipped_K, float K_flip, float lambda, float nu,
        int *t_calcs, long **active_node_count, curandGenerator_t generator, int threads_per_block);
__global__ void spreading_step (
        int *lin_position, int *lin_network, float *lin_network_weight,
        int node_count, float threshold, float lambda, float nu,
        float *random_numbers, int *was_active, int *is_active);
int find_init_node (int node_count, int *network, int *position);
__global__ void set_init_node (int node_count, int *was_active, int init_node);
float calc_flipped_threshold (float threshold);
void reset_d (int len, double **array);
void reset_l (int len, long **array);
int file_exists_ (char *filename);


int main (int argc, char **argv) {
    struct arguments arguments;

    /* Default values for command line options */

    // lambda: Activation probability
    arguments.from_lambda = 1.0;
    arguments.to_lambda = 1.0;
    arguments.delta_lambda = 0.025;
    // nu: Deactivation probability
    arguments.from_nu = 0.1;
    arguments.to_nu = 0.1;
    arguments.delta_nu = 0.025;
    // theta: Threshold
    arguments.from_theta = 0.25;
    arguments.to_theta= 0.25;
    arguments.delta_theta = 0.025;
    // rho: Edge probability
    arguments.from_rho = 0.25;
    arguments.to_rho = 0.25;
    arguments.delta_rho = 0.025;
    // kappa: Inhibitory edge probability
    arguments.from_kappa = 0.0;
    arguments.to_kappa = 0.0;
    arguments.delta_kappa = 0.025;
    arguments.device = 0;
    arguments.repeat = 1e2;
    arguments.steps = 1e6;
    arguments.threads_per_block = 128;
    // Verbosity controls
    //arguments.quiet, verbose;

    argp_parse(&argp, argc, argv, 0, 0, &arguments);

    printf("ARG1 = %s\nVERBOSE = %s\nQUIET = %s\nFROM LAMBDA = %f\nTO LAMBDA = %f\n",
            arguments.args[0],
            arguments.verbose ? "yes" : "no",
            arguments.quiet ? "yes" : "no", arguments.from_lambda, arguments.to_lambda);

    if (!file_exists_(arguments.args[ARG_FILENAME])) {
        AsciiUtils_fatal_error_msg("File doesn't exist");
        return EXIT_FAILURE;
    }

    // lambda: activation probability
    float lambda;
    // nu: deactivation probability
    float nu;
    // theta: Threshold
    float theta;

    // TODO: rename K to theta, add argparser magic
    // time dependent threshold model
    // K_flip: use flipped_K as threshold value with K_flip probability
    // Use negative value for constant threshold model K_flip = -1;s
    float K_flip = -1.0, flipped_K;

    // rho: Edge probability
    // e.g 0.8 means
    // 80% undirected (added both ways),
    // 20% directed (edge added only in one direction)
    float rho;
    // kappa: Inhibitory edge probability
    float kappa;  // 0 if there are no inhibitory nodes.


    // TODO: MAKE SURE ALL PARSING RULES ARE KEPT
    //int parse_failed = ArgparseUtils_parse(argc, argv, &filename, &cuda_device);
    //if (parse_failed)
    //    return EXIT_FAILURE;


    AsciiUtils_welcome();

    cudaSetDevice(arguments.device);
    Write_init(arguments.device);

    for (rho = arguments.to_rho; arguments.from_rho <= rho; rho -= arguments.delta_rho) {
        // Read in network
        int node_count, edge_count;
        int **in_network;
        float **in_network_weight;
        Read_network(arguments.args[ARG_FILENAME], rho, kappa, &node_count, &edge_count,
                     &in_network, &in_network_weight);
        // Linearise network
        int *position;
        int *network;
        float *network_weight;
        Linearise_network(node_count, edge_count,
                          in_network, in_network_weight,
                          &position, &network, &network_weight);
        // Free original network
        for (int j = 0; j < node_count; j++) {
            free(in_network[j]);
            free(in_network_weight[j]);
        }
        free(in_network);
        free(in_network_weight);
        // Copy variables from host to device
        int *d_position;
        cuSafe(cudaMalloc((void **) &d_position, node_count * sizeof(int)));
        cuSafe(cudaMemcpy((void *) d_position,
                          (void *) position,
                          node_count * sizeof(int),
                          cudaMemcpyHostToDevice));
        int *d_network;
        cuSafe(cudaMalloc((void **) &d_network, (node_count + edge_count) * sizeof(int)));
        cuSafe(cudaMemcpy((void *) d_network,
                          (void *) network,
                          (node_count + edge_count) * sizeof(int),
                          cudaMemcpyHostToDevice));
        float *d_network_weight;
        cuSafe(cudaMalloc((void **) &d_network_weight, (node_count + edge_count) * sizeof(float)));
        cuSafe(cudaMemcpy((void *) d_network_weight,
                          (void *) network_weight,
                          (node_count + edge_count) * sizeof(float),
                          cudaMemcpyHostToDevice));

        // Calculate and Store values only at relevant times
        // TODO: move it to separate function, too noisy.
        //   1. Get length of time arrays
        int t_length = 0;
        for (int j = 0, tt = 0, prev = 0; tt < arguments.steps; j++) {
            tt = (int) (pow(1.08,(double)j));
            if (tt > prev) {
                t_length++;
                prev = tt;
            }
        }
        //   2. Create time array
        int *t_calcs;
        t_calcs = (int *) malloc(t_length * sizeof(int));
        //   3. Fill time array
        for (int j = 0, tt = 0, prev = 0, i = 0; tt < arguments.steps; j++) {
            tt = (int) (pow(1.08,(double)j));
            if (tt > prev) {
                prev = tt;
                t_calcs[i++] = prev;
            }
        }
        // Create variables to store relevant calculated results
        long *active_node_count, *active_node_count_sum, *survived_sum, *avalanche;
        active_node_count = (long *) malloc(t_length * sizeof(long));
        active_node_count_sum = (long *) malloc(t_length * sizeof(long));
        survived_sum = (long *) malloc(t_length * sizeof(long));
        avalanche = (long *) malloc(arguments.repeat * sizeof(long));
        double *avg_active_node_count, *survival_probability;
        avg_active_node_count = (double *) malloc(t_length * sizeof(double));
        survival_probability = (double *) malloc(t_length * sizeof(double));
        // Create random generator so it'll be reused throughout the program
        curandGenerator_t generator = RandomUtils_create_generator();
        // Start simulations
        for (theta = arguments.to_theta; arguments.from_theta <= theta; theta -= arguments.delta_theta) {
            flipped_K = calc_flipped_threshold(theta);
            for (nu = arguments.to_nu; arguments.from_nu <= nu; nu -= arguments.delta_nu) {
                for (lambda = arguments.from_lambda; lambda <= arguments.to_lambda; lambda += arguments.delta_lambda) {
                    // Reset arrays for results from different parameters
                    reset_l(t_length, &active_node_count_sum);
                    reset_l(t_length, &survived_sum);
                    reset_d(t_length, &avg_active_node_count);
                    reset_d(t_length, &survival_probability);
                    reset_l(arguments.repeat, &avalanche);
                    // Simulate spreading arguments.repeat times
                    // GpuTimer timer; timer.start();
                    for (int i = 0; i < arguments.repeat; i++) {
                        reset_l(t_length, &active_node_count);
                        int init_node = find_init_node(node_count, network, position);
                        spreading(node_count, edge_count, init_node,
                                  d_position, d_network, d_network_weight, arguments.steps,
                                  theta, flipped_K, K_flip, lambda, nu,
                                  t_calcs, &active_node_count, generator, arguments.threads_per_block);
                        for (int tt = 0; tt < t_length; tt++) {
                            active_node_count_sum[tt] += active_node_count[tt];
                            survived_sum[tt] += !!active_node_count[tt];
                            int interval_length = active_node_count[tt] - (!tt ? 0 : active_node_count[tt-1]);
                            avalanche[i] += active_node_count[tt] * interval_length;
                        }
                    }
                    // timer.stop(); then timer.elapsed()/1e3 in seconds
                    for (int tt = 0; tt < t_length; tt++) {
                        avg_active_node_count[tt] = (double) active_node_count_sum[tt] / (double) arguments.repeat;
                        survival_probability[tt] = (double) survived_sum[tt] / (double) arguments.repeat;
                    }
                    // TODO: MAKE SURE NAMING ARE CONSISTENT IN OTHER FILES, TOO, eg. T_MAX, K theta
                    Write_active_node_count_average(avg_active_node_count, t_calcs, t_length,
                                                    lambda, nu, theta, rho, arguments.steps, arguments.repeat);
                    Write_survival_probability(survival_probability, t_calcs, t_length, lambda, nu, theta, rho,
                                               arguments.steps, arguments.repeat);
                    Write_avalanche_distribution(avalanche, arguments.repeat, lambda, nu, theta, rho,
                                                 arguments.steps, arguments.repeat);
                }
            }
        }
        // Clean up
        RandomUtils_destroy_generator(generator);
        cuSafe(cudaFree(d_position));
        cuSafe(cudaFree(d_network));
        cuSafe(cudaFree(d_network_weight));
        free(position);
        free(network);
        free(network_weight);
        free(active_node_count);
        free(active_node_count_sum);
        free(survived_sum);
        free(avalanche);
        free(avg_active_node_count);
        free(survival_probability);
    }

    AsciiUtils_success();
    return EXIT_SUCCESS;
}


void spreading(int node_count, int edge_count, int init_node,
               int *d_position, int *d_network, float *d_network_weight, int steps,
               float K, float flipped_K, float K_flip, float lambda, float nu,
               int *t_calcs, long **active_node_count, curandGenerator_t generator, int threads_per_block) {
    int *h_was_active;
    h_was_active = (int *) malloc(node_count * sizeof(int));
    int *d_was_active;
    cuSafe(cudaMalloc((void **) &d_was_active, node_count * sizeof(int)));
    int *d_is_active;
    cuSafe(cudaMalloc((void **) &d_is_active, node_count * sizeof(int)));
    float *d_random_numbers;
    cuSafe(cudaMalloc((void **) &d_random_numbers, node_count * sizeof(float)));
    const int BLOCKS_FOR_NODES = min((node_count + threads_per_block - 1)/threads_per_block, 65535);
    set_init_node<<<BLOCKS_FOR_NODES, threads_per_block>>>(node_count, d_was_active, init_node);

    GpuTimer timer;
    timer.start();

    for (int t = 0, tt = 0; t < steps; t++) {
        curandSafe(curandGenerateUniform(generator, d_random_numbers, node_count));

        if (t == t_calcs[tt]) {
            cuSafe(cudaMemcpy((void *) h_was_active,
                              (void *) d_was_active,
                              node_count * sizeof(int),
                              cudaMemcpyDeviceToHost));

            int c = 0;
            for (int i = 0; i < node_count; i++)
                if (h_was_active[i])
                    c++;
            if (!c)
                break;
            (*active_node_count)[tt++] = c;
        }

        float threshold = RandomUtils_get_random_float() < K_flip ? flipped_K : K;
        spreading_step<<<BLOCKS_FOR_NODES, threads_per_block>>>(
            d_position, d_network, d_network_weight,
            node_count, threshold, lambda, nu,
            d_random_numbers, d_was_active, d_is_active
        );

        cuSafe(cudaMemcpy((void *) d_was_active,
                          (void *) d_is_active,
                          node_count * sizeof(int),
                          cudaMemcpyDeviceToDevice));
    }
    // // timer.stop(); timer.elapsed()/1e3; // in seconds
    // Clean Up
    cuSafe(cudaFree(d_is_active));
    cuSafe(cudaFree(d_was_active));
    cuSafe(cudaFree(d_random_numbers));
    free(h_was_active);
}


__global__ void spreading_step (
    int *lin_position, int *lin_network, float *lin_network_weight,
    int node_count, float threshold, float lambda, float nu,
    float *random_numbers, int *was_active, int *is_active
) {
    int i = threadIdx.x + blockIdx.x * blockDim.x;
    while (i < node_count) {
        float r = random_numbers[i];
        float s = 0;  //  weighted sum of active neighbors
        int lim = lin_position[i] + lin_network[lin_position[i]];
        for (int j = lin_position[i] + 1; j <= lim; j++) {
            int to_node = lin_network[j];
            if (was_active[to_node])
                s += lin_network_weight[j];
            if (threshold <= s)
                break;
        }
        if (was_active[i])
            is_active[i] = (r <= nu) ? NODE_INACTIVE : NODE_ACTIVE;
        else
            is_active[i] = (r <= lambda && threshold <= s) ? NODE_ACTIVE : NODE_INACTIVE;
        i += blockDim.x * gridDim.x;
    }
}


int find_init_node(int node_count, int *network, int *position) {
    do {
        int r = RandomUtils_get_random_long(node_count - 1);
        if (network[position[r]])
            return r;
    } while (1);
}


__global__ void set_init_node (int node_count,int *was_active, int init_node) {
    int i = threadIdx.x + blockIdx.x * blockDim.x;
    while (i < node_count) {
        if (i == init_node)
            was_active[i] = NODE_ACTIVE;
        else
            was_active[i] = NODE_INACTIVE;
        i += blockDim.x * gridDim.x;
    }
}


float calc_flipped_threshold (float threshold) {
    // You might create a more interesting model...
    return 1 - threshold;
}


void reset_d(int len, double **array) {
    for (int i = 0; i < len; i++)
        (*array)[i] = 0;
}


void reset_l(int len, long **array) {
    for (int i = 0; i < len; i++)
        (*array)[i] = 0;
}

int file_exists_ (char *filename) {
    // http://stackoverflow.com/questions/230062/whats-the-best-way-to-check-if-a-file-exists-in-c-cross-platform
    // R_OK from unistd.h
    return access(filename, R_OK) != -1;
}