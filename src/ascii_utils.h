#ifndef ASCII_UTILS_H_INCLUDED
#define ASCII_UTILS_H_INCLUDED


#include <stdio.h>


void AsciiUtils_welcome();
void AsciiUtils_loading();
void AsciiUtils_success();
void AsciiUtils_fatal_error();
void AsciiUtils_fatal_error_msg(char *msg);


void AsciiUtils_success() {
    printf("\nYAY, SUCCESS\n");
    printf("\\o/\n");
    printf(" | \n");
    printf("/ \\\n\n");
}


void AsciiUtils_fatal_error_msg(char *msg) {
    fprintf(stderr, "\n _____\n");
    fprintf(stderr, "/     \\\n");
    fprintf(stderr, "| o O |\n");
    fprintf(stderr, "\\  ^  /\n");
    fprintf(stderr, " ||||| \n\n");
    fprintf(stderr, "ERROR\n%s\n\n", msg == NULL ? "Unknown" : msg);
}


void AsciiUtils_fatal_error() {
    AsciiUtils_fatal_error_msg(NULL);
}


void AsciiUtils_loading() {
    // source: http://ascii.co.uk/art/hourglass
    printf("\n+=====+\n");
    printf("|(.;.)|\n");
    printf("| )Y( |\n");
    printf("|(.:.)|\n");
    printf("+=====+\n\n");
}


void AsciiUtils_welcome() {
    // source: http://ascii.co.uk/art/dogs
    printf("\nGET THE DACHSHUND PARTY STARTED\n=\\_____|  =\\_____/  =\\_____,\n");
    printf("  \"   \"     \"   \"     \"   \"\n");
}


#endif