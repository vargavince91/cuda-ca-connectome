#!/bin/sh

docker run \
    --rm \
    --interactive \
    --user="$(id -u):$(id -g)" \
    --net=none \
    --volume "$PWD":/plots \
    cuda-ca-connectome/python-plot \
    /bin/sh -c "python3 /plots/'$@'"
