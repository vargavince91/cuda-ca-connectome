"""
In this script we shortly provide a comparison between
the original C++ code and the GPU optimzed CUDA C code.

Lambda values change from 80-100% while the other parameter values were fixed:
* nu = 0.950
* K = 0.250
* T = 10000
* R = 10000
"""

import matplotlib.pyplot as plt
import pylab

import colors
import utils

CUDA = [
    (utils.get_data_filename('cuda/0.80.csv'), '$80,0$%', colors.RED),
    (utils.get_data_filename('cuda/0.85.csv'), '$85,0$%', colors.DARK_BLUE),
    (utils.get_data_filename('cuda/0.90.csv'), '$90,0$%', colors.ORANGE),
    (utils.get_data_filename('cuda/0.95.csv'), '$95,0$%', colors.GREEN),
    (utils.get_data_filename('cuda/1.00.csv'), '$100,0$%', colors.YELLOW),
]

CPP = [
    (utils.get_data_filename('cpp/0.80.dat'), '$80,0$%', colors.RED),
    (utils.get_data_filename('cpp/0.85.dat'), '$85,0$%', colors.DARK_BLUE),
    (utils.get_data_filename('cpp/0.90.dat'), '$90,0$%', colors.ORANGE),
    (utils.get_data_filename('cpp/0.95.dat'), '$95,0$%', colors.GREEN),
    (utils.get_data_filename('cpp/1.00.dat'), '$100,0$%', colors.YELLOW)
]

fig = plt.figure()
ax = plt.subplot(111)
ax.set_xlim(1,9999)
ax.set_ylim(1e-4, 2e-1)
ax.set_yscale('log')
ax.set_xscale('log')

position = ax.get_position()
ax.set_position([position.x0, position.y0,
                 position.width * 0.85, position.height])

for filename, label, color in CUDA:
    data = pylab.loadtxt(filename)
    t = data[:,0]
    Pt =  data[:,1]
    ax.plot(t, Pt, label=label, color=color, linestyle='solid', linewidth=1.5)

for filename, label, color in CPP:
    data = pylab.loadtxt(filename)
    # Make sure two format's indices are correct
    data = data[1:]
    t = data[:,0] - 2
    Pt =  data[:,2]
    ax.plot(t, Pt, linestyle='solid', linewidth=0.5, color=color,
            marker='p', markerfacecolor='white', markeredgecolor=color,
            markeredgewidth=2, markevery=(2, 9))

handles, labels = ax.get_legend_handles_labels()
labels, handles = zip(*sorted(zip(labels, handles), reverse=True,
                      key=lambda t:
                          float(t[0].strip('$').strip('$%').replace(',', '.'))))

ax.legend(handles, labels, loc='center left', bbox_to_anchor=(1, 0.5))
plt.xlabel('$t$', fontsize=18)
plt.ylabel('$P(t)$', fontsize=18)

utils.save_plot(plt)
