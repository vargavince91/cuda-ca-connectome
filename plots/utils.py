"""
This file contains simple reusable, utility functions with terrible names.
"""

import os
import sys
# http://stackoverflow.com/a/13240524/4541492
# It says:
#       __main__.__file__ works in Python 2.7 but not in 3.2
# but for me it worked with a simple import, too.
import __main__ as main

def main_script_name():
    """
    Get the main script name without extension and directories.
    """
    # Simple os.path.abspath(main.__file__).rsplit('.py', 1)[0]
    # didn't work in Docker, but there must be a better way
    # to retrieve a filename from a path. I'll look it up later.
    return os.path.abspath(main.__file__).rsplit(os.sep, 1)[1].rsplit('.py', 1)[0]

def get_data_filename(filename):
    """
    The directory where the datafiles required for running the plotter scripts
    are is called as the main script without extension,
    e.g. example.py's data directory would be example/.
    """
    return os.path.join(main_script_name(), filename)

def get_plot_dest_filename(output_format, dest_folder='dest'):
    """
    Create the appropriate filename for the plots.
    Stitch it together!
    """
    dir_abs_path = os.path.dirname(os.path.abspath(__file__))
    filename = main_script_name() + '.' + output_format
    return os.path.join(dir_abs_path, dest_folder, filename)

def save_plot(plt):
    """
    Take care of saving the plots in all desired output formats.
    """
    for output_format in output_formats():
        plt.savefig(get_plot_dest_filename(output_format))

def output_formats():
    """
    All plotter scripts accept the desired output formats
    as command line arguments.
    If command line argument is not specified, default to {'svg', 'pdf'}
    e.g. run: `python plotter_script_name.py svg pdf if_invalid_skip_it`
    """
    default = {'svg', 'pdf'}
    supported = {'png', 'pdf', 'ps', 'eps', 'svg'}
    formats = set(sys.argv[1:])
    return {f for f in formats if f in supported} if formats else default
