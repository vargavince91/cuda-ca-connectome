"""
This script creates a plot showing how changing the
`THREADS_PER_BLOCK` value affects the time necessary
for executing computations.
"""
# TODO: Add benchmark description
import matplotlib.pyplot as plt
import numpy as np
import pylab

import colors
import utils

DATA_TO_PLOT = [
    (utils.get_data_filename('PC1.dat'), 'PC 1', colors.RED),  # PC gergo1
    (utils.get_data_filename('PC2.dat'), 'PC 2', colors.DARK_BLUE)   # PC odor6
]

fig, ax = plt.subplots()

ax.set_xlim(2**4+1, 2**11-1)
ax.set_ylim(41, 109)
ax.set_xscale('log', basex=2)

for filename, label, color in DATA_TO_PLOT:
    data = pylab.loadtxt(filename)
    threads_per_block = data[:, 0]
    worst_time = np.amax(data[:, 1])  # 100%
    time_as_percentage = data[:, 1] * 100 / worst_time
    best_time = np.amin(time_as_percentage)
    best_threads_per_block = np.argmin(time_as_percentage)
    # Plot all data points within data set
    plt.plot(threads_per_block, time_as_percentage,
             label=label, color=color,
             marker='o', markersize=12.5, markerfacecolor=color,
             linewidth=0.75, linestyle='dotted')
    # Plot best data point with a big green bubble
    plt.plot(threads_per_block[best_threads_per_block], best_time,
             marker='o', markersize=19.5,
             color=colors.GREEN, linewidth=0)

plt.legend(loc='best')
plt.title('')
plt.ylabel('$t/t_{0}$ [%]', fontsize=18)
plt.xlabel('THREADS_PER_BLOCK', fontsize=14, family='monospace')

utils.save_plot(plt)

