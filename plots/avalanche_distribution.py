"""
This script displays the avalanche size distribution.

The used parameter values are
* lambda = 0.800
* nu = 1.000
* K = 0.250
* T = 10000
* R = 50000

"""

import matplotlib.pyplot as plt
import numpy as np
import pylab

import utils

filename = utils.get_data_filename('s-ps.dat')
data = pylab.loadtxt(filename)

x_range = (0.5, 4e5)
y_range = (1.5e-5, 1.5)
fig, ax = plt.subplots()
ax.set_xlim(*x_range)
ax.set_ylim(*y_range)
ax.set_yscale('log')
ax.set_xscale('log')
R = 5e4  # The calculation was repeated 50 000 times.

s = data[:, 0]
ps = data[:, 1]/R

plt.scatter(s, ps, linewidths=.5, s=(np.log10(ps)+5.5)**2.5*8)

plt.xlabel('$s$')
plt.ylabel('$P(s)$')
plt.grid()

utils.save_plot(plt)
