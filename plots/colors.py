"""
The module contains HEX colors that I use for plotting.
"""

ORANGE = "#F46036"
YELLOW = "#FFBC42"
RED = "#E71D36"
DARK_BLUE = "#2E294E"
LIGHT_BLUE = "#73D2DE"
GREEN = "#00BA00"
