"""
Display average active node count as a function of time for different rho values.

Used params:
* lambda = 0.800
* nu = 0.800
* K = 0.250
* T = 10000
* R = 10000

"""

import matplotlib.pyplot as plt
import pylab

import colors
import utils

DATA_TO_PLOT = [
    (utils.get_data_filename('rho-0.250.dat'), '$25$%', colors.DARK_BLUE),
    (utils.get_data_filename('rho-0.500.dat'), '$50$%', colors.ORANGE),
    (utils.get_data_filename('rho-0.750.dat'), '$75$%', colors.GREEN),
    (utils.get_data_filename('rho-1.000.dat'), '$100$%', colors.RED),
]

fig = plt.figure()
ax = plt.subplot(111)
ax.set_xlim(1,9999)
ax.set_ylim(9e-3, 0.9)
ax.set_yscale('log')
ax.set_xscale('log')

for filename, label, color in DATA_TO_PLOT:
    data = pylab.loadtxt(filename)
    t = data[:,0]
    alpha_t =  data[:,1]
    ax.plot(t, alpha_t, label=label, color=color, linestyle='solid', linewidth=1.5)

plt.legend(loc='best')
plt.xlabel('$t$', fontsize=18)
plt.ylabel(r'$\bar{\alpha}(t)$', fontsize=18)

utils.save_plot(plt)