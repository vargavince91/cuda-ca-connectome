"""
Show how the survival probability changes over time
for different lambda parameter values

Used parameter values are:
* nu = 0.950
* K = 0.250
* T = 10000
* R = 10000
"""

import math
import matplotlib.pyplot as plt
import numpy as np
import pylab

import colors
import utils

DATA_TO_PLOT = [
    (utils.get_data_filename('lambda-0.700.csv'), '$70,0$%', colors.GREEN, 'solid'),
    (utils.get_data_filename('lambda-0.725.csv'), '$72,5$%', colors.GREEN, 'dashed'),
    (utils.get_data_filename('lambda-0.750.csv'), '$75,0$%', colors.GREEN, 'dashdot'),
    (utils.get_data_filename('lambda-0.775.csv'), '$77,5$%', colors.GREEN, 'dotted'),
    (utils.get_data_filename('lambda-0.800.csv'), '$80,0$%', colors.RED, 'solid'),
    (utils.get_data_filename('lambda-0.825.csv'), '$82,5$%', colors.RED, 'dashed'),
    (utils.get_data_filename('lambda-0.850.csv'), '$85,0$%', colors.RED, 'dashdot'),
    (utils.get_data_filename('lambda-0.875.csv'), '$87,5$%', colors.RED, 'dotted'),
    (utils.get_data_filename('lambda-0.900.csv'), '$90,0$%', colors.DARK_BLUE, 'solid'),
    (utils.get_data_filename('lambda-0.925.csv'), '$92,5$%', colors.DARK_BLUE, 'dashed'),
    (utils.get_data_filename('lambda-0.950.csv'), '$95,0$%', colors.DARK_BLUE, 'dashdot'),
    (utils.get_data_filename('lambda-0.975.csv'), '$97,5$%', colors.DARK_BLUE, 'dotted'),
    (utils.get_data_filename('lambda-1.000.csv'), '$100,0$%', colors.YELLOW, 'solid')
]

fig = plt.figure()
ax = plt.subplot(111)
ax.set_xlim(1,9999)
ax.set_ylim(1e-4, 2e-1)
ax.set_yscale('log')
ax.set_xscale('log')

position = ax.get_position()
ax.set_position([position.x0, position.y0,
                 position.width * 0.85, position.height])

for filename, label, color, linestyle in DATA_TO_PLOT:
    data = pylab.loadtxt(filename)

m = []
for filename, label, color, linestyle in DATA_TO_PLOT:
    data = pylab.loadtxt(filename)
    t = data[:,0]
    Pt =  data[:,1]
    from_index = int(10**(math.log(len(t), 10)/3))
    to_index = int(10**(2*math.log(len(t), 10)/3))
    trunk_t = t[from_index:to_index]
    trunk_Pt = Pt[from_index:to_index]
    logt = np.log(trunk_t)
    logPt = np.log(trunk_Pt)
    coeffs = np.polyfit(logt, logPt, deg=1)
    m.append(coeffs[0])
    poly = np.poly1d(coeffs)
    yfit = lambda x: np.exp(poly(np.log(x)))
    ax.plot(t, Pt, label=label, color=color, linestyle=linestyle, linewidth=1.5)

handles, labels = ax.get_legend_handles_labels()
labels, handles = zip(*sorted(zip(labels, handles), reverse=True,
                      key=lambda t:
                          float(t[0].strip('$').strip('$%').replace(',', '.'))))
ax.legend(handles, labels, loc='center left', bbox_to_anchor=(1, 0.5))
plt.xlabel('$t$', fontsize=18)
plt.ylabel('$P(t)$', fontsize=18)

utils.save_plot(plt)

