'''
Show how the survival probability changes over time
for different K threshold parameter values

Other parameter values were fixed:
* nu = 0.950
* lambda = 0.950
* T = 10000
* R = 10000
'''
import matplotlib.pyplot as plt
import pylab

import colors
import utils

DATA_TO_PLOT = [
    (utils.get_data_filename('K0.250.csv'), '$25,0$%', colors.GREEN, 'solid'),
    (utils.get_data_filename('K0.292.csv'), '$29,2$%', colors.GREEN, 'dashed'),
    (utils.get_data_filename('K0.333.csv'), '$33,3$%', colors.YELLOW, 'solid'),
    (utils.get_data_filename('K0.375.csv'), '$37,5$%', colors.YELLOW, 'dashed'),
    (utils.get_data_filename('K0.500.csv'), '$50,0$%', colors.ORANGE, 'solid'),
    (utils.get_data_filename('K0.625.csv'), '$62,5$%', colors.DARK_BLUE, 'solid'),
    (utils.get_data_filename('K0.750.csv'), '$75,0$%', colors.LIGHT_BLUE, 'solid')
]

fig = plt.figure()
ax = plt.subplot(111)
ax.set_xlim(1, 9999)
ax.set_ylim(1e-4, 2e-1)
ax.set_yscale('log')
ax.set_xscale('log')

position = ax.get_position()
ax.set_position([position.x0, position.y0,
                 position.width * 0.85, position.height])

for filename, label, color, linestyle in DATA_TO_PLOT:
    data = pylab.loadtxt(filename)
    ax.plot(data[:, 0], data[:, 1],
            label=label, color=color,
            linestyle=linestyle, linewidth=1.5)

ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))
plt.xlabel('$t$', fontsize=18)
plt.ylabel('$P(t)$', fontsize=18)

utils.save_plot(plt)

