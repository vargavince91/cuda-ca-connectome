# Plots for `cuda-ca-connectome`

## Create plots

You can execute the scripts that create the plots either locally or using a Docker Container.

### Using Docker

1. [Install Docker](https://docs.docker.com/engine/installation/)
    If you are using Ubuntu, you can use the short bash script I created (can't guarantee it's bulletproof). You need root privileges.
    
    ```
    $ bash ./install-docker.sh
    ```
    
    To enable managing Docker as a non-root user and starting Docker on boot, run
    
    ```
    # Manage docker as a non-root user
    $ bash ./non-root-docker.sh
    # Start docker on boot
    $ sudo systemctl enable docker
    ```
    
2. Build the image
    
    ```
    $ docker build . --tag="cuda-ca-connectome/python-plot"
    ```
    
    To see if the image is successfully created, run
    
    ```
    $ docker images
    REPOSITORY         TAG       IMAGE ID    CREATED          SIZE
    cuda-ca-connectome/python-plot    latest    c0ffe3      8 seconds ago    961 MB
    ```

3. [optional] Make the shell script executable and add it to your path.
4. You can now use the `docker-python.sh`

    ```
    $ bash docker-python.sh exec_time_n_threads.py
    ```

### Using locally installed Python

This list assumes you are using Linux.

1. Make sure you have Python and [pip](https://packaging.python.org/) installed.

    ```
    python --version && pip --version
    ```

2. Install matplotlib

    ```
    $ apt install python3-matplotlib
    ```

3. Install pip dependencies

    ```
    $ pip install -r dependencies.txt
    ```

4. Run a plotter script, e.g. `python exec_time_n_threads.py`

You could also install it in a virtualenv.

## Remaining tasks

* [ ] Give better explanation for the plots. Explain what's going on and why. Explain what's the meaning of the parameters
* [ ] Update parameter names, once refactoring finished on CUDA C code
* [ ] Some of my plot scripts were created while finishing my thesis. Code quality is obviously not the best. I promise I'll clean this up one day :)
